0. Installer VirtualBox
1. Telecharger l'ISO OpenMediaVault:
    => https://sourceforge.net/projects/openmediavault/
2. Formater par openmediavault via image ISO 
    2.1/ Hostname: HomeServer
3. Changer la configuration réseau sur VirtualBox en  (Accès par Pont)
4. Add 2*hard Drive
5. configurer RAID1 (Miroring) à partir de ces deux disque ajoutés
6. Créer FS dédier à un user
7. Créer le dossier partager 
8. Créer l'user 
9. Activer le service de partage
10. Installer omv-extras:
    
    10. 1. à exécuter sur le powershell en mode administrateur:

        > ssh root@HommeServer
        
        > wget -O - https://github.com/OpenMediaVault-Plugin-Developers/packages/raw/master/install | bash
        
11. Installer Docker via omv-extras
12. Installer Portainer via omv-extras
13. Changer le repo de template App sur Portainer: 
    => https://raw.githubusercontent.com/Qballjos/portainer_templates/master/Template/template.json
14. installer NextCloud
15. Add volume on nextCloud Container via Portainer -en mode Bind et non pas un volume existant- (exemple):

| Host/volume  | Path in container |
|--|--|
| /srv/dev-disk-by-uuid-ea1cdb8d-0a0c-4f36-a623-3dade4c4b46f/DataSoufiane | /Drive |
 	
16. Changer les droits système sur le partage créé:
    > chmod -R 0777 /srv/dev-disk-by-uuid-ea1cdb8d-0a0c-4f36-a623-3dade4c4b46f/DataSoufiane/
17. Active Storage externe on nextCloud
18. Add this drive for the spessific user sur la section Admin (exemple):

    | mon disque  |   Local   |   Aucun   |   /Drive  |   (Ali) |

19. Voir les autre application d'edition qu'on peux installer sur NextCloud et utiliser(ex: Office )
20. Connexion à Internet:
    Dynamic DNS avec DuckDNS
    
    OR

    avec : remote.it
21. Comunication server (ex Whatsapp/Telegram/Signal)
    => Documentation  

        https://computingforgeeks.com/run-synapse-matrix-homeserver-in-docker-containers/
        
        https://matrix.org/docs/legacy/understanding-synapse-hosting/
